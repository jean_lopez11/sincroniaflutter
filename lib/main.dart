import 'package:asincronia/services/mockapi.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    // ignore: prefer_const_constructors
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: const MyHomePage(title: 'Asincronia'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key, required this.title});

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  double width1 = 0;
  double width2 = 0;
  double width3 = 0;
  int numero1 = 0;
  int numero2 = 0;
  int numero3 = 0;

  void getFerrariInteger() async => {
        width1 = 300,
        await MockApi().getFerrariInteger().then((value) => {
              setState(() {
                numero1 = value;
                width1 = 0;
              })
            })
      };

  void getHyundaiInteger() async => {
        width2 = 300,
        await MockApi().getHyundaiInteger().then((value) => {
              setState(() {
                numero2 = value;
                width2 = 0;
              })
            })
      };

  void getFisherPriceInteger() async => {
        width3 = 300,
        await MockApi().getFisherPriceInteger().then((value) => {
              setState(() {
                numero3 = value;
                width3 = 0;
              })
            })
      };

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Center(child: Text(widget.title)),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Column(
                  children: [
                    Ink(
                      decoration: const ShapeDecoration(
                        color: Colors.green,
                        shape: CircleBorder(),
                      ),
                      child: Padding(
                        padding: const EdgeInsets.all(10),
                        child: IconButton(
                          icon: const Icon(
                            Icons.flash_on,
                            color: Colors.black,
                          ),
                          iconSize: 50,
                          onPressed: () {
                            setState(() {
                              getFerrariInteger();
                            });
                          },
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(6.0),
                      child: AnimatedContainer(
                        duration: const Duration(seconds: 1),
                        width: width1,
                        height: 10,
                        decoration: const BoxDecoration(color: Colors.green),
                      ),
                    ),
                    Text(
                      numero1.toString(),
                      style: const TextStyle(
                        fontSize: 50,
                        color: Colors.green,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ],
                ),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Column(
                  children: [
                    Ink(
                      decoration: const ShapeDecoration(
                        color: Colors.orange,
                        shape: CircleBorder(),
                      ),
                      child: Padding(
                        padding: const EdgeInsets.all(10),
                        child: IconButton(
                          icon: const Icon(
                            Icons.airport_shuttle,
                            color: Colors.black,
                          ),
                          iconSize: 50,
                          onPressed: () {
                            setState(() {
                              getHyundaiInteger();
                            });
                          },
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(6.0),
                      child: AnimatedContainer(
                        duration: const Duration(seconds: 3),
                        width: width2,
                        height: 10,
                        decoration: const BoxDecoration(color: Colors.orange),
                      ),
                    ),
                    Text(
                      numero2.toString(),
                      style: const TextStyle(
                        fontSize: 50,
                        color: Colors.orange,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ],
                ),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Column(
                  children: [
                    Ink(
                      decoration: const ShapeDecoration(
                        color: Colors.red,
                        shape: CircleBorder(),
                      ),
                      child: Padding(
                        padding: const EdgeInsets.all(10),
                        child: IconButton(
                          icon: const Icon(
                            Icons.directions_walk,
                            color: Colors.black,
                          ),
                          iconSize: 50,
                          onPressed: () {
                            setState(() {
                              getFisherPriceInteger();
                            });
                          },
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(6.0),
                      child: AnimatedContainer(
                        duration: const Duration(seconds: 10),
                        width: width3,
                        height: 10,
                        decoration: const BoxDecoration(color: Colors.red),
                      ),
                    ),
                    Text(
                      numero3.toString(),
                      style: const TextStyle(
                        fontSize: 50,
                        color: Colors.red,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}

