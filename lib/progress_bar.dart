import 'package:flutter/material.dart';

class ProgressBar extends StatelessWidget {

  final int value;
  final int totalValue;
  final Color color;
  const ProgressBar({super.key, required this.value, required this.totalValue, required this.color});
  @override
  Widget build(BuildContext context) {
    double width=200;
    double ratio = value/totalValue;
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
          Icon(Icons.timer), 
          SizedBox(width: 5), 
          Stack(children: [
              Container(
                  width: 200,
                  height: 10,
                  // decoration: BoxDecoration(color: Colors.green, borderRadius: BorderRadius.circular(5)),
                ),
                Material(
                  elevation: 3,
                  borderRadius: BorderRadius.circular(5),
                  child: AnimatedContainer(
                    duration: Duration(seconds: 1), 
                    width:  width*ratio,
                    height: 10,
                    
                    decoration: BoxDecoration(
                      color: color, 
                      borderRadius: BorderRadius.circular(5)
                    ),
                    ),
                )
            ],
          )
        ],
    );
}
  
}
